package ru.jackson.jackson.api.controller;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.jackson.jackson.api.type.ArticleType;
import ru.jackson.jackson.api.type.PeopleType;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class JacksonController {

    @GetMapping("/generate")
    public void generateJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        PeopleType author = new PeopleType(204L, "Roadster", 32, "male");

        LocalDateTime articleCreated = LocalDateTime.parse("2015-05-22T14:56:29.058");
        LocalDateTime articleUpdated = LocalDateTime.parse("2019-05-22T17:56:28.000");

        ArticleType article = new ArticleType(18L, "Title of 18", "Body of 18", articleCreated, articleUpdated, author);

        System.out.println(article);

        try (JsonGenerator generator =
                     mapper.getFactory().createGenerator(
                             new File("src/main/resources/json/article.json")
                             , JsonEncoding.UTF8)) {

            generator.writeStartObject();

            generator.writeFieldName("data");
            generator.writeStartArray();
            generator.writeStartObject();
            generator.writeStringField("type", ArticleType.TYPE);
            generator.writeNumberField("id", article.getId());

            generator.writeFieldName("attributes");
            generator.writeStartObject();
            generator.writeStringField("title", article.getTitle());
            generator.writeStringField("body", article.getBody());
            generator.writeStringField("created", article.getCreatedString());
            generator.writeStringField("updated", article.getUpdatedString());
            generator.writeEndObject();

            generator.writeFieldName("relationships");
            generator.writeStartObject();
            generator.writeFieldName("author");
            generator.writeStartObject();
            generator.writeFieldName("data");
            generator.writeStartObject();
            generator.writeNumberField("id", article.getAuthor().getId());
            generator.writeStringField("type", PeopleType.TYPE);
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndArray();

            generator.writeFieldName("included");
            generator.writeStartArray();
            generator.writeStartObject();
            generator.writeStringField("type", PeopleType.TYPE);
            generator.writeNumberField("id", article.getAuthor().getId());
            generator.writeFieldName("attributes");
            generator.writeStartObject();
            generator.writeStringField("name", article.getAuthor().getName());
            generator.writeNumberField("age", article.getAuthor().getAge());
            generator.writeStringField("gender", article.getAuthor().getGender());
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndArray();

            generator.writeEndObject();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/parse")
    public void parseJson() throws IOException {
        boolean parentAdded = false;
        List<String> parents = new ArrayList<>();
        String key = null;

        PeopleType author = new PeopleType();
        ArticleType article = new ArticleType();
        article.setAuthor(author);

        String pathName = "src/main/resources/json/article_source.json";

        try (JsonParser parser = new JsonFactory()
                .createParser(new File(pathName));) {

            while (!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();

                if (jsonToken == null) {
                    continue;
                }

                switch (jsonToken) {
                    case START_ARRAY:
                    case START_OBJECT:
                        if (key != null && !parentAdded) {
                            parents.add(key);
                            parentAdded = true;
                        }
                        break;

                    case FIELD_NAME:
                        key = parser.getCurrentName();
                        parentAdded = false;

                        if (key.equals("id")) {
                            long id = parser.nextLongValue(-1);

                            if (parents.size() == 1) {
                                article.setId(id);
                            } else if (parents.size() == 6) {
                                author.setId(id);
                            }
                        }

                        if (key.equals("title")) {
                            article.setTitle(parser.nextTextValue());
                        }

                        if (key.equals("body")) {
                            article.setBody(parser.nextTextValue());
                        }

                        if (key.equals("created")) {
                            String created = parser.nextTextValue();
                            article.setCreated(ArticleType.convertUtcStringToLocalDateTime(created));
                        }

                        if (key.equals("updated")) {
                            String updated = parser.nextTextValue();
                            article.setCreated(ArticleType.convertUtcStringToLocalDateTime(updated));
                        }

                        if (key.equals("name")) {
                            author.setName(parser.nextTextValue());
                        }

                        if (key.equals("age")) {
                            author.setAge(parser.nextIntValue(-1));
                        }

                        if (key.equals("gender")) {
                            author.setGender(parser.nextTextValue());
                        }

                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(parents);
        System.out.println(article);
    }
}
