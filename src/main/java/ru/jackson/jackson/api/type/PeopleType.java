package ru.jackson.jackson.api.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeopleType {
    public static final String TYPE = "people";

    private long id;
    private String name;
    private int age;
    private String gender;
}
