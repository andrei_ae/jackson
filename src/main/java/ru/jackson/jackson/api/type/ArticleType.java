package ru.jackson.jackson.api.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleType {
    public static final String TYPE = "articles";

    private long id;
    private String title;
    private String body;
    private LocalDateTime created;
    private LocalDateTime updated;
    private PeopleType author;

    public String getCreatedString() {
        return convertDateTimeToString(created);
    }

    public String getUpdatedString() {
        return convertDateTimeToString(updated);
    }

    private String convertDateTimeToString(LocalDateTime dateTime) {
        return dateTime.toString() + "Z";
    }

    public static LocalDateTime convertUtcStringToLocalDateTime(String time) {
        return LocalDateTime.parse(time, DateTimeFormatter.ISO_DATE_TIME);
    }
}
